/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class SimpleFileIO {
    
    public static void main(String[] args) throws Exception {
        //This way of writing to file is only good for one time use, if I were to do this all over again
        //EVERYTHING would be rewritten
        PrintWriter out = new PrintWriter( new FileWriter( "OutFile.txt") );
        
        //These three lines are written to the file
        out.println("this is a line in my file ");
        out.println("this is a second line in my file...");
        out.println("this is a third line in my file...");
        out.flush();//Makes sure the lines get wrote to the file! Nothing pending
        out.close();//Closes the resource
        
        //Create a new Scanner object which takes in a new bufferedreader and a new filereader to read a 
        //specific file you pass into the paramaters
        Scanner sc = new Scanner( new BufferedReader( new FileReader( "OutFile.txt" ) ) );
        
        //A while loop to condition check...if the file has a line written to it, jump into the while loop
        //and execute the code
        while( sc.hasNextLine() ){
            
            //Create a new variable to store the lines from the file into
            String currentLine = sc.nextLine();
            //Print out those lines to the console
            System.out.println( currentLine );
              
        }
        
        System.out.println("That's all the content in the file");
        
        
        
    }
}
