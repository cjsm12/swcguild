/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

/**
 *
 * @author user
 */
public class CommentingCode {
    
    public static void main(String[] args) {
        
        //Comments are written to explain code in a easily understandable way
        //Basically for single lines, anything after a // is considered a comment
        System.out.println("Normal code is compiled and runs ..."); 
        System.out.println("Comments however ...");
        
        //Comments can be on their own line
        System.out.println("...");// or they can share like this
        
        //However if you put the //BEFORE a line of code
        //System.out.println("Then it is considered a comment, and wont execute"!);
        
        /*
        
        This is a multi line comment!
        named, because it expands SO many line!
        
        
        */
    }
    
    
}
