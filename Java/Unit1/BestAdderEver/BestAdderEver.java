/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class BestAdderEver {
    
    
    public static void main(String[] args) {
        
    //Declaration AND initialization 
    int myHeight;
    double myWeight;
    double myGoalWeight;
    double total;
    
    //Create scanner object
    Scanner sc = new Scanner(System.in);
    
    //Getting user input
        System.out.println("What is your height? ");
        myHeight = sc.nextInt();
        
        System.out.println("What is your weight? ");
        myWeight = sc.nextDouble();
        
        System.out.println("What is your goal weight? ");
        myGoalWeight = sc.nextDouble();
        
       
        //Output users input
        System.out.printf("Your height, weight, and goal weight are as follow:\n %s: %d\n%s: %d\n%s: %d",
                           "Height(inches)",myHeight,
                           "Weight(pounds)",myWeight,
                           "Goal Weight", myGoalWeight);
        
      //Add up the users input
      total = myHeight + myWeight + myGoalWeight;
      
      //Display total
        System.out.println("\nIf you were to add your height, weight, and goal weight together as a unified"
                + "number, you would come up with a value of " + total);
        
        //Display total again
        System.out.println(total);
    
    }
    
    
    
    
    
    
    
}
