/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Random;

/**
 *
 * @author user
 */
public class ALittleChaos {
    
    
    public static void main(String[] args) {
        
        
        Random rn = new Random();
        
        System.out.println("Random can make integers: " + rn.nextInt());
        System.out.println("Or a double: " + rn.nextDouble());
        System.out.println("Or even a boolean: " + rn.nextBoolean());
        
        int num = rn.nextInt(100);
        
        System.out.println("You can store a randomized result " + num);
        System.out.println("And use it over and over again " + num + ", " + num );
        
        
        System.out.println("Or just keep generating new numbers");
        System.out.println("Here is a bunch of random numbers between 0 and 100 ");
        
        System.out.print(rn.nextInt(101) + ", ");
        System.out.print(rn.nextInt(101) + ", ");
        System.out.print(rn.nextInt(101) + ", ");
        System.out.print(rn.nextInt(101) + ", ");
        System.out.print(rn.nextInt(101) + ", ");
        System.out.print(rn.nextInt(101) + ", ");
        System.out.print(rn.nextInt(101) + ", ");
        System.out.println(rn.nextInt(101));
        
        //rn.nextInt( 50 ) + 50 will generate a number between 49 and 99 because
        //instead of generating a number between 0 and 49 like it would have been
        //with just the 50 as the argument, when you add the 50 onto it it changes
        //the starting number to start generating from, from 0 to 49 to 49 to 99

        
        
    }
}
