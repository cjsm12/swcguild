/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class LuckySevens {
    
    public static void main(String[] args) {
        
        int start = 1;
        int initMoney = 0;
        int rollsBeforeMoneyGone = 0;
        int maxMoneyHeld = 0;
        int rollsMaxMoney = 0;
        int diceSum = 0;
        
        Scanner sc = new Scanner( System.in );
        
        System.out.println("How much money do you have to bet? ");
        initMoney = sc.nextInt();
        
        while( start == 1 ){
            
            if( initMoney > maxMoneyHeld ){
                
                maxMoneyHeld = initMoney;
            }
            
            diceSum = rollDice();
            
            if( diceSum == 7 ){
                
            initMoney += 7;
            rollsBeforeMoneyGone++;
            rollsMaxMoney++;
              
            } else {
                
                initMoney -= 1;
                rollsBeforeMoneyGone++;
                
                if( initMoney <= 0 )
                    start--;
                
            }
            
           
        }
        
        System.out.print("Oh no! The game is over! ");
        System.out.print("You are broke after " + rollsBeforeMoneyGone + " rolls.");
        System.out.print("You should have quit after " + rollsMaxMoney + " rolls when you had $" + maxMoneyHeld);

        
    }
    
    public static int rollDice(){
        
        int dice1;
        int dice2;
        int totalOfDice;
        
        Random rn = new Random();
        
        dice1 = rn.nextInt( 6 ) + 1;
        dice2 = rn.nextInt( 6 ) + 1;
        
        totalOfDice = dice1 + dice2;
        
        return totalOfDice;
        
        
    }
    
    
        
        
        
    }
    
    
  
