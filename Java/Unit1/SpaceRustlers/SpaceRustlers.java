/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

/**
 *
 * @author user
 */
public class SpaceRustlers {

    public static void main(String[] args) {
        
        int spaceShips = 10;
        int aliens = 25;
        int cows = 100;
        
        if( aliens > spaceShips ){
            
            System.out.println("Vroom Vroom! Let's get going! ");
            
        } else{
            System.out.println("There aren't enough green guys to drive these ships!");
        }
        
            if( cows == spaceShips ){
            
            System.out.println("Wow, way to plan ahead! JUST enough room for all these walking hamburgers");
            
        }   else if ( cows > spaceShips ){
            
            System.out.println("Dangit! I don't know how we are going to fit all these cows in here!"); 
            
        } else{
            System.out.println("Too many ships! Not enough cows");
        }
            if( aliens > cows ){
                
                System.out.println("Hurrah, we've got the hamburger grub! Hamburger party on Alpha Centauri ");
                
            } else if( cows >= aliens ){
                
                System.out.println("Oh no! The herd got restless and took over! Looks like_we're_ hamburgers now!");
                
            }
        
        //2) The else if and if are condition checkers, so it checks to see if a condition is true,
        //and if true is the case, then the code following the if statement will be executed.
        //Now the else if statement usually follows an existing if statemnt that does additional
        //checking of a similar value if and only if the initial if statement is false. If else is good
        //if you want to check multiple values and not just have one if statement followed by an ending
        //else statement
        
        //3 If the else statement is removed while the else if is true, nothing will be displayed
        //because the else if statement is true and that is what will be displayed, however if the
        //else if statement is false, then the else will still not run and the program would be wrong
        
        
    }


}
