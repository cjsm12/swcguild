/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class MiniZork {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner( System.in );
        
        System.out.println("You are driving down the road back home from work "
                + "and suddenly an emergency broadcat comes on the radio... ");
        System.out.printf("%s", "\u001B[1m Attention! A Plague has been released onto us by"
                + " a foreign threat and is now airborne. PLEASE GET UNDERGROUND IMMEDIATELY!");
        System.out.println("\nYou are still 45 minutes from your house...What do you do?\n");
        System.out.println("1) Take the risk and drive home    2) Take the next exit to the underground shelter");
        String userAction = sc.nextLine();
        
        if( userAction.equalsIgnoreCase("take the risk and drive home" ) ){
            
            System.out.println("You decide to risk it and dive home...");
            System.out.println("10 minutes later you see the sky begin to turn red");
            System.out.println("Your car suddenly starts spudding out and comes to a slow halt.");
            System.out.println("The red sky is approaching fast and you start to feel the wind and rain.");
            System.out.println("You see off to the distance a large open field with a house that may or may not have a basement?");
            System.out.println("You also remembered you have your jump starting kit in the trunk that you could try and continue home.\n");
            System.out.println("1) Run for the house    2) Jump start the car");
            userAction = sc.nextLine();
            
            if(userAction.equalsIgnoreCase( "run for the house" ) ){
                
                System.out.println("\nYou get out and start running for the house...");
                System.out.println("The wind and rain starts pouring down and a funny smell begins to tickle your senses, so youput your shirt over your nose.");
                System.out.println("You get to the house and begin knocking hard on the door...");
                System.out.println("No answer is recieved after several attempts and now the wind is beginning to overpower you...");
                System.out.println("You think to yourself, maybe there is a cellar in the back?\n" );
                System.out.println("1) Run to the back of the house    2) Break the front window to get in he house");
                userAction = sc.nextLine();
            }
            
            if(userAction.equalsIgnoreCase( "run to the back of the house" ) ){
                
                System.out.println("\nYou go to run to the back of the house and you begin to feel dizzy...");
                System.out.println("You are short of breath and can't walk anymore...you fall to the ground");
                System.out.println("DEAD...by the plague.");
         
            }else if( userAction.equalsIgnoreCase( "break the front window to get in he house" ) ){
                
                System.out.println("\nYou take the pocket knife out of your back pocket and use the back side to break the glass.");
                System.out.println("You are able to get into the house and begin to feel a little better.");
                System.out.println("You are safer...but not safe...yet");
                
            }
          
        }else if(userAction.equalsIgnoreCase( "jump start the car" ) ){
            
            System.out.println("\nYou get out of the car to get to your trunk...");
            System.out.println("ERRRRRR HONK HONK...");
            System.out.println("You didn't look before getting out of the car and a car smashes into you...");
            System.out.println("Instantly killing you as you go skidding across the road looking like a smashed tomato");
            
            
        }else if(userAction.equalsIgnoreCase( "take the next exit to the underground shelter" ) ){
            
            System.out.println("\nYou cut off the guy behind you as you hurry and take the exit last second");
            System.out.println("You see the sky as it begins to turn red and start increasing your speed");
            System.out.println("You pass a sign that reads emergency underground shelter 10 miles, then...");
            System.out.println("You come to a SCREEECHING halt as you slam on your brakes!");
            System.out.println("You forgot to take into account that you live in a city of 3.4 million people");
            System.out.println("The bumper to bumper line of cars extends for miles is seems...");
            System.out.println("You can wait here for the traffic to move along as the suspicious angry sky begins to move closer and closer,");
            System.out.println("or you can get out and try on foot?\n");
            System.out.println("1) Wait here     2) Try on foot");
            userAction = sc.nextLine();
            
                if( userAction.equalsIgnoreCase( "wait here" ) ){
                    
                    System.out.println("\nTime passes by and you begin to see people leaving their cars...");
                    System.out.println("You turn around and see the reasoning why, aliens like creatures begin falling from the F*%$*!& SKY!!!");
                    System.out.println("You try to get out of the car and run but the creatures are already swarming you...");
                    System.out.println("You suffer a long and gruesome death as the creatures with their 30 inch stingers begin piercing your body like if you were made of air");
                    
                    
                }else if( userAction.equals( "try on foot" ) ){
                    
                    System.out.println("\nYou hurry out of the car and begin heading there on foot...");
                    System.out.println("As you are walking, a motorcyclist comes beside you before coming to a stop,");
                    System.out.println(" the person says to come with them before the deadly sky approaches");
                    System.out.println("Deadly sky? You think to yourself...you begin to worry if this person is correct and that the sky actually is deadly.");
                    System.out.println("Your fear begins to overwhelm you and you take the offer from this stranger");
                    System.out.println("The motorcyclist begins weaving in and out of the cars as you both are met with shouts and honks from people stuck in the traffic");
                    System.out.println("You seem to be making it, then your cell phone starts to ring...");
                    System.out.println("TO BE CONTINUED");
                }
            
        }
        
        
        
        
    }
    
}
