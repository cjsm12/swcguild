/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class WindowMaster {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        //Variables used for user input
        float windowHeight;
        float windowWidth;
        
        //Variables used to take in user input as a string
        String stringWindowHeight;
        String stringWindowWidth;
        
        //Variables used to calculate area and perimeter
        float windowArea;
        float windowPerimeter;
        
        //Variable used to calculate total cost of the glass and trim seperately
        float glassCost;
        float trimCost;
        
        //Variable used to calculate totalCost
        float totalCost;
        
        
        System.out.println("What is the height of the window? ");
        stringWindowHeight = sc.nextLine();
        windowHeight = Float.parseFloat(stringWindowHeight);
        
        System.out.println("What is the window width? ");
        stringWindowWidth = sc.nextLine();
        windowWidth = Float.parseFloat(stringWindowWidth);
        
        windowArea = windowWidth * windowHeight;
        System.out.printf("The area of the window you requested is %.2f feet\n", windowArea);
        
        windowPerimeter = 2 * ( windowArea );
        System.out.printf("The perimeter of the window is %.2f feet\n", windowPerimeter);
        
        
        glassCost = 3.50f * ( windowArea );
        trimCost = 2.25f * ( windowArea );
        
        System.out.printf("Glass and Trim Cost: \n%s: $%.2f\n%s: $%.2f\n", 
                "Glass Cost", glassCost, 
                "Trim Cost", trimCost);
        
        totalCost = glassCost + trimCost;
        
        System.out.printf("Total cost: $%.2f", totalCost);
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
}
